"use strict";

$(function() {
    $('#blogCarousel').carousel({
        interval: 10000
    });

    $('#membersCarousel').carousel({
        interval: 10000
    });
});
