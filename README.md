# Task 3: HTML/CSS
## Description
Open charity is an organisation that hosts events in London about open source software and solution for charities. The proposed design of the new site is as follows:

https://invis.io/EN7L8CK2P#/168991599_OC_Site005b

Assets can be downloaded from here:

https://bitbucket.org/compucorp/open-charity-design

## Instructions
The site must be responsive
The site should work fine with the latest version of Chrome, Firefox, Safari, Edge and with IE11
If you are also able to host the solution simply then please do. We recommend using AWS free tier but reasonable hosting costs will be reimbursed.
Extra brownie points!
Use a CSS preprocessor
Build the site as a Drupal template that can be installed on a Drupal site (NOTE: this is mandatory for Route 2 applicants.)

### Live
https://task-03.herokuapp.com/

### Technologies used

* Grunt
* Bower
* LESS
* HTML5
* CSS3
* Bootstrap


### To run local
It’s A Piece Of :cake:


Just clone: `git clone git@gitlab.com:compucorp-tests/task3.git`

Enter the project folder: `cd task3`

Install the dev dependencies: `npm install`

Run grunt local: `grunt dev`
